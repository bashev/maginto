package com.udari.helpers;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiManager;
import com.intellij.psi.impl.source.xml.XmlFileImpl;
import com.intellij.psi.search.FilenameIndex;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.search.GlobalSearchScopes;
import com.intellij.psi.xml.XmlDocument;
import com.intellij.psi.xml.XmlFile;
import com.intellij.psi.xml.XmlTag;
import com.intellij.util.containers.HashMap;
import com.jetbrains.php.PhpIndex;
import com.jetbrains.php.lang.psi.elements.PhpClass;
import com.udari.UdariSettings;
import com.udari.model.MagentoRoute;
import com.udari.model.MagentoTheme;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Map;

/**
 * @author B G Kavinga on 2/23/16.
 */
public class Magento {

    protected static Map<Project, Magento> instanceByProject;

    protected Project project;

    protected Magento(Project project) {

        this.project = project;
    }

    public static Magento getInstance(Project project) {
        if (instanceByProject == null) {
            instanceByProject = new HashMap<Project, Magento>();
        }
        if (!instanceByProject.containsKey(project)) {
            Magento instance = new Magento(project);
            instanceByProject.put(project, instance);
        }
        return instanceByProject.get(project);
    }

    public static ArrayList<String> getAvailableModules(Project project) {
        ArrayList<String> modules = new ArrayList<String>();
        UdariSettings settings = UdariSettings.getInstance(project);
        // Searching inside app/code directory
        String relativePath = settings.getRelativePathToMage(project) + "app/code";
        VirtualFile searchScope = project.getBaseDir().findFileByRelativePath(relativePath);
        GlobalSearchScope scope = null;
        PsiFile psiFiles[] = {};
        if (searchScope != null) {
            scope = GlobalSearchScopes.directoryScope(project, searchScope, true);
            psiFiles = FilenameIndex.getFilesByName(project, "module.xml", scope);
        }
        for (PsiFile psiFile : psiFiles) {
            XmlDocument doc = ((XmlFileImpl) psiFile).getDocument();
            if (doc.getRootTag() != null && doc.getRootTag().findFirstSubTag("module") != null) {
                XmlTag tag = doc.getRootTag().findFirstSubTag("module");
                if (tag != null) {
                    String moduleName = tag.getAttributeValue("name");
                    modules.add(moduleName);
                }
            }

        }
        Collections.sort(modules);
        return modules;
    }

    public static ArrayList<String> getAvailableCollections(Project project) {
        ArrayList<String> collections = new ArrayList<String>();
        UdariSettings settings = UdariSettings.getInstance(project);
        String relativePath = settings.getRelativePathToMage(project) + "app/code";
        VirtualFile searchScope = project.getBaseDir().findFileByRelativePath(relativePath);
        GlobalSearchScope scope = null;
        PsiFile psiFiles[] = {};
        if (searchScope != null) {
            scope = GlobalSearchScopes.directoryScope(project, searchScope, true);
            PhpIndex index = PhpIndex.getInstance(project);
            Collection<PhpClass> classes = index.getClassesByNameInScope("Collection", scope);
            for (PhpClass clazz : classes) {
                if (clazz.getSuperFQN().equals("\\Magento\\Framework\\Model\\ResourceModel\\Db\\Collection\\AbstractCollection")) {
                    collections.add(clazz.getPresentableFQN());
                }
            }

        }
        return collections;
    }

    public static ArrayList<String> getHandlersByModule(Project project, String companyName, String moduleName) {
        ArrayList<String> handlers = new ArrayList<String>();
        UdariSettings settings = UdariSettings.getInstance(project);
        // Searching inside app/code directory
        String relativePath = settings.getRelativePathToMage(project) + "app/code/" + companyName + "/" + moduleName + "/view/adminhtml/layout";
        VirtualFile searchScope = project.getBaseDir().findFileByRelativePath(relativePath);
        GlobalSearchScope scope = null;
        Collection<VirtualFile> layoutFiles = Collections.EMPTY_LIST;
        if (searchScope != null) {
            scope = GlobalSearchScopes.directoryScope(project, searchScope, true);
            layoutFiles = FilenameIndex.getAllFilesByExt(project, "xml", scope);
        }
        for (VirtualFile layout : layoutFiles) {
            PsiFile psiFile = PsiManager.getInstance(project).findFile(layout);
            if (psiFile instanceof XmlFileImpl) {
                XmlFile layoutXml = (XmlFile) psiFile;
                if (layoutXml.getRootTag().getName().equals("page")) {
                    handlers.add(layout.getName());
                }
            }

        }

        return handlers;
    }

    public static ArrayList<MagentoTheme> getAvailableThemes(Project project) {
        ArrayList<MagentoTheme> magentoThemes = new ArrayList<MagentoTheme>();
        UdariSettings settings = UdariSettings.getInstance(project);
        // Searching inside app/design/theme directory for custom themes
        String relativePath = settings.getRelativePathToMage(project) + "app/design/frontend";
        VirtualFile searchScope = project.getBaseDir().findFileByRelativePath(relativePath);
        GlobalSearchScope scope = null;
        PsiFile psiFiles[] = {};
        if (searchScope != null) {
            scope = GlobalSearchScopes.directoryScope(project, searchScope, true);
            psiFiles = FilenameIndex.getFilesByName(project, "theme.xml", scope);
        }
        for (PsiFile psiFile : psiFiles) {
            XmlDocument doc = ((XmlFileImpl) psiFile).getDocument();
            if (doc.getRootTag() != null && doc.getRootTag().findFirstSubTag("title") != null) {
                XmlTag tag = doc.getRootTag().findFirstSubTag("title");
                if (tag != null) {
                    String themeName = tag.getValue().getText();
                    MagentoTheme magentoTheme = new MagentoTheme();
                    magentoTheme.setName(themeName);
                    magentoTheme.setVirtualFile(psiFile.getVirtualFile());
                    magentoThemes.add(magentoTheme);
                }
            }

        }
        Collections.sort(magentoThemes);
        return magentoThemes;

    }

    public static ArrayList<MagentoRoute> getAdminRoutesByModule(Project project, String companyName, String moduleName) {
        ArrayList<MagentoRoute> routes = new ArrayList<MagentoRoute>();
        UdariSettings settings = UdariSettings.getInstance(project);
        // Searching inside app/code directory
        String relativePath = settings.getRelativePathToMage(project) + "app/code/" + companyName + "/" + moduleName + "/etc/adminhtml/routes.xml";
        VirtualFile routeFile = project.getBaseDir().findFileByRelativePath(relativePath);
        if (routeFile != null) {
            PsiFile routePsiFile = PsiManager.getInstance(project).findFile(routeFile);
            if (routePsiFile instanceof XmlFileImpl) {
                XmlFile routeXml = (XmlFile) routePsiFile;
                for (XmlTag tag : routeXml.getRootTag().getSubTags()) {
                    if (tag.getName().equals("router")) {
                        XmlTag routeTag = tag.findFirstSubTag("route");
                        if (routeTag != null) {
                            MagentoRoute route = new MagentoRoute();
                            route.setRouteId(routeTag.getAttributeValue("id"));
                            route.setRouteName(routeTag.getAttributeValue("frontName"));
                            routes.add(route);
                        }
                    }
                }

            }
        }

        return routes;
    }

    public static VirtualFile getFileByPath(Project project, String path) {
        String parts[] = path.split(project.getBasePath());
        if (parts.length > 0) {
            path = parts[1];
        }
        VirtualFile file = project.getBaseDir().findFileByRelativePath(path);
        return file;
    }
}
