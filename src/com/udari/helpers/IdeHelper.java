package com.udari.helpers;


import com.intellij.notification.Notification;
import com.intellij.notification.NotificationType;
import com.intellij.notification.Notifications;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.diagnostic.Logger;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import com.intellij.util.messages.MessageBus;
import com.intellij.util.ui.UIUtil;
import com.sun.istack.internal.Nullable;
import com.udari.UdariIcons;

import javax.swing.*;

/**
 * @author B G Kavinga on 2/21/16.
 */
public class IdeHelper {

    public static void logError(String message) {
        message += " (if you think this is a bug please send the trace to gkavinga@gmail.com)";
        Logger.getInstance("").error(message);
    }

    public static void showDialog(final Project project, final String message, final String title, final Icon icon) {
        UIUtil.invokeAndWaitIfNeeded(new Runnable() {
            @Override
            public void run() {
                Messages.showMessageDialog(project, message, title, icon);
            }
        });
    }

    public static void showDialog(Project project, String message, String title) {
        showDialog(project, message, title, UdariIcons.UDARI_ICON_16x16);
    }

    public static void showNotification(String message, NotificationType type, @Nullable Project project) {
        final MessageBus messageBus = project == null ? ApplicationManager.getApplication().getMessageBus() : project.getMessageBus();

        final Notification notification = new Notification("MaGinto Notification", "MaGinto Notification", message, type, null);

        ApplicationManager.getApplication().executeOnPooledThread(new Runnable() {
            @Override
            public void run() {
                messageBus.syncPublisher(Notifications.TOPIC).notify(notification);
            }
        });
    }
}
