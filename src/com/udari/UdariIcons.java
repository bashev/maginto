package com.udari;

import com.intellij.openapi.util.IconLoader;

import javax.swing.*;

/**
 * @author B G Kavinga on 2/21/16.
 */
public class UdariIcons {
    public static Icon UDARI_ICON_16x16 = IconLoader.getIcon("/icons/magento.png");
    public static Icon MAGENTO_ICON_16x16 = IconLoader.getIcon("/icons/magento.png");
}
