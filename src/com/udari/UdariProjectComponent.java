package com.udari;

import com.intellij.openapi.components.ProjectComponent;
import com.intellij.openapi.project.Project;
import org.jetbrains.annotations.NotNull;

/**
 * @author B G Kavinga on 2/21/16.
 */
public class UdariProjectComponent implements ProjectComponent {

    private Project project;


    public UdariProjectComponent(Project project) {
        this.project = project;
    }

    public String getDefaultPathToMagento() {
        String projectPath = project.getBaseDir().getPath();
        if (projectPath == null || projectPath.isEmpty()) {
            // projectPath = _project.getLocation();
            projectPath = project.getPresentableUrl();  // getBasePath()
        }
        return projectPath;
    }


    public void initComponent() {
        // initialize project components
    }


    public static UdariProjectComponent getInstance(Project project) {
        return project.getComponent(UdariProjectComponent.class);
    }


    public void disposeComponent() {
        // called when dispose
    }

    public void projectOpened() {
        // called when project is opened
    }

    public void projectClosed() {
        // called when project is being closed
    }

    public void clearAllCache() {
        // TODO: remove all cached files
    }

    @NotNull
    public String getComponentName() {

        return "UdariProjectComponent";
    }


}
