package com.udari.ui.dialog;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.ValidationInfo;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class NewModuleDialog extends DialogWrapper {
    private JPanel contentPane;
    private JTextField companyName;
    private JTextField moduleName;


    protected Project project;

    public NewModuleDialog(Project project) {
        super(project);
        this.project = project;
        init();
        setTitle("New Module");
    }

    @Override
    protected JComponent createCenterPanel() {

        return contentPane;
    }

    public String getCompanyName() {
        return companyName.getText();
    }

    public String getModuleName() {

        return moduleName.getText();
    }

    @Override
    public JComponent getPreferredFocusedComponent() {

        return companyName;
    }

    @Nullable
    protected ValidationInfo doValidate() {
        if (moduleName.getText().equals("")) {
            return new ValidationInfo("Module name should not be empty", moduleName);
        }
        if (companyName.getText().equals("")) {
            return new ValidationInfo("Company name should not be empty", companyName);
        }
        return null;
    }
}
