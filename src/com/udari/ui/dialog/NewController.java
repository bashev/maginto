package com.udari.ui.dialog;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.ValidationInfo;
import com.udari.helpers.Magento;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.util.ArrayList;

public class NewController extends DialogWrapper {
    private JPanel contentPanel;
    private JComboBox moduleList;
    private JComboBox area;
    private JTextField routeId;
    private JTextField group;

    protected Project project;

    public NewController(Project project) {
        super(project);
        this.project = project;
        init();
        setTitle("Controller route");
    }

    public void init() {
        super.init();
        ArrayList<String> modules = Magento.getAvailableModules(this.project);
        for (String module : modules) {
            moduleList.addItem(module);
        }
        area.addItem("Frontend");
        area.addItem("Adminhtml");

    }

    public String getModuleName() {
        String selectedModule = (String) moduleList.getSelectedItem();
        String parts[] = selectedModule.split("_");
        return parts[1];

    }

    public String getCompanyName() {
        String selectedModule = (String) moduleList.getSelectedItem();
        String parts[] = selectedModule.split("_");
        return parts[0];

    }

    public String getGroup() {
        return group.getText();
    }

    public String getArea() {
        return area.getSelectedItem().toString();
    }

    public String getRouteId() {
        return routeId.getText();
    }

    @Override
    protected JComponent createCenterPanel() {

        return contentPanel;
    }

    @Nullable
    protected ValidationInfo doValidate() {
        if (getModuleName().equals("")) {
            return new ValidationInfo("Model name should not be empty", moduleList);
        }
        if (getGroup().equals("")) {
            return new ValidationInfo("Group should not be empty", group);
        }
        if (getArea().equals("")) {
            return new ValidationInfo("Area should not be empty", area);
        }
        if (getRouteId().equals("")) {
            return new ValidationInfo("MagentoRoute should not be empty", routeId);
        }
        return null;
    }


}
