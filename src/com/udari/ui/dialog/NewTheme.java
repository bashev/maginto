package com.udari.ui.dialog;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.udari.helpers.Magento;
import com.udari.model.MagentoTheme;

import javax.swing.*;
import java.util.ArrayList;

public class NewTheme extends DialogWrapper {
    private JPanel contentPane;
    private JTextField companyName;
    private JTextField themeName;
    private JComboBox parentTheme;

    protected Project project;

    public NewTheme(Project project) {
        super(project);
        this.project = project;
        init();
        setTitle("New Theme");
    }

    public void init() {
        super.init();
        // populate magentoThemes
        ArrayList<MagentoTheme> magentoThemes = Magento.getAvailableThemes(project);
        for (MagentoTheme magentoTheme : magentoThemes) {
            parentTheme.addItem(magentoTheme);
        }

    }

    public MagentoTheme getParentTheme() {
        if (parentTheme.getSelectedItem() instanceof MagentoTheme) {
            return (MagentoTheme) parentTheme.getSelectedItem();
        }
        return null;
    }
    public String getCompanyName() {
        return companyName.getText();
    }

    public String getThemeName() {

        return themeName.getText();
    }


    @Override
    protected JComponent createCenterPanel() {

        return contentPane;
    }

    @Override
    public JComponent getPreferredFocusedComponent() {

        return companyName;
    }
}
