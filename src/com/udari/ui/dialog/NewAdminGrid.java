package com.udari.ui.dialog;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.udari.helpers.Magento;
import com.udari.model.MagentoRoute;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class NewAdminGrid extends DialogWrapper {
    private JPanel contentPanel;
    private JComboBox moduleList;
    private JComboBox collectionList;
    private JComboBox routerGroups;
    private JTextField group;

    protected Project project;

    public NewAdminGrid(Project project) {
        super(project);
        this.project = project;
        init();
        setTitle("Admin Grid");
    }

    public void init() {
        super.init();
        ArrayList<String> modules = Magento.getAvailableModules(this.project);
        for (String module : modules) {
            moduleList.addItem(module);
        }

        ArrayList<String> collections = Magento.getAvailableCollections(this.project);

        for (String collection : collections) {
            collectionList.addItem(collection);
        }
        updateRoutes();
        moduleList.addActionListener(new ModuleChangeListener(this));

    }

    public void updateRoutes() {
        ArrayList<MagentoRoute> routes = Magento.getAdminRoutesByModule(project, getCompanyName(), getModuleName());
        routerGroups.removeAllItems();
        for (MagentoRoute route : routes) {
            routerGroups.addItem(route);
        }
        routerGroups.setEditable(true);
    }

    @Override
    protected JComponent createCenterPanel() {

        return contentPanel;
    }

    public String getModuleName() {
        String selectedModule = (String) moduleList.getSelectedItem();
        String parts[] = selectedModule.split("_");
        return parts[1];

    }

    public String getCompanyName() {
        String selectedModule = (String) moduleList.getSelectedItem();
        String parts[] = selectedModule.split("_");
        return parts[0];

    }

    public MagentoRoute getSelectedRoute() {
        Object selected = routerGroups.getSelectedItem();
        if (selected instanceof MagentoRoute) {
            return (MagentoRoute) selected;
        }
        if (selected instanceof String) {
            MagentoRoute route = new MagentoRoute();
            route.setRouteName(selected.toString());
            route.setRouteId(selected.toString());
            route.setNew(true);
            return route;
        }
        return null;
    }

    public String getGroup() {
        return group.getText();
    }


    public String getCollection() {
        if (collectionList.getSelectedItem() != null) {
            collectionList.getSelectedItem().toString();
        }
        return null;
    }


    protected class ModuleChangeListener implements ActionListener {

        protected NewAdminGrid grid;

        public ModuleChangeListener(NewAdminGrid grid) {
            this.grid = grid;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            grid.updateRoutes();
        }
    }
}
