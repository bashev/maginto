package com.udari.ui.dialog;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

/**
 * @author B G Kavinga on 4/24/16.
 */
public class TestActionDialog extends DialogWrapper {

    private Project myProject;
    private JPanel myRootPanel;

    public TestActionDialog(Project project) {
        super(project, false);
        myProject = project;
        init();
        setTitle("My Test Action");
    }

    @Nullable
    @Override
    protected JComponent createCenterPanel() {
        return myRootPanel;
    }

}
