package com.udari.actions;

import com.intellij.ide.actions.CreateElementActionBase;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.DataContext;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.actionSystem.Presentation;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.components.ServiceManager;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiDirectory;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.codeStyle.CodeStyleManager;
import com.intellij.util.ArrayUtil;
import com.udari.UdariSettings;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

/**
 * @author B G Kavinga on 4/25/16.
 */
public abstract class AbstractAction extends CreateElementActionBase {

    private static final PsiElement[] CANCELED = new PsiElement[1];
    protected PsiElement[] updatedElements = PsiElement.EMPTY_ARRAY;
    protected String pathToMagento;
    protected Project project;
    protected VirtualFile selectedFile;
    protected DataContext dataContext;


    public AbstractAction(String text, String description, @Nullable Icon icon) {
        super(text, description, icon);
    }

    @NotNull
    @Override
    protected PsiElement[] invokeDialog(Project project, PsiDirectory directory) {
        final PsiElement[] psiElements = invokeDialogImpl(project, directory);
        return psiElements == CANCELED ? PsiElement.EMPTY_ARRAY : psiElements;
    }

    public void update(final AnActionEvent e) {
        super.update(e);

        final Presentation presentation = e.getPresentation();
        if (presentation.isEnabled()) {
            UdariSettings settings = ServiceManager.getService(e.getProject(), UdariSettings.class);
            if (e.getProject() != null &&
                    settings != null &&
                    settings.getPathToMagento() != null &&
                    !settings.getPathToMagento().equals("")) {

                pathToMagento = settings.getPathToMagento();
                project = e.getProject();
                dataContext = e.getDataContext();
                selectedFile = PlatformDataKeys.VIRTUAL_FILE.getData(dataContext);
                return;
            }

            presentation.setEnabled(false);
            presentation.setVisible(false);
        }
    }

    protected void reformatFile(final PsiFile psiFile) {
        if (psiFile != null) {
            final CodeStyleManager codeStyleManager = CodeStyleManager.getInstance(project);
            WriteCommandAction.runWriteCommandAction(project, new Runnable() {
                @Override
                public void run() {
                    codeStyleManager.reformat(psiFile);
                }
            });
        }
    }

    protected PsiElement addUpdatedElement(PsiElement element) {
        if (element != null) {
            updatedElements = ArrayUtil.append(updatedElements, element);
            return element;
        }
        return null;
    }

    public void openFile(VirtualFile file) {
        if (file != null) {
            FileEditorManager.getInstance(project).openFile(file, true);
        }
    }

    protected abstract PsiElement[] invokeDialogImpl(Project project, PsiDirectory directory);

    @NotNull
    @Override
    protected PsiElement[] create(String s, PsiDirectory psiDirectory) throws Exception {
        return new PsiElement[0];
    }

    @Override
    protected String getErrorTitle() {
        return null;
    }

    @Override
    protected String getCommandName() {
        return null;
    }

    @Override
    protected String getActionName(PsiDirectory psiDirectory, String s) {
        return null;
    }
}
