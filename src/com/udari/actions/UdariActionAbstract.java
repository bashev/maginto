package com.udari.actions;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.DataContext;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiFile;
import com.intellij.psi.codeStyle.CodeStyleManager;
import com.udari.UdariSettings;
import com.udari.helpers.IdeHelper;

/**
 * @author B G Kavinga on 2/21/16.
 */
public abstract class UdariActionAbstract extends AnAction {

    protected AnActionEvent event;

    protected DataContext dataContext;

    protected Project project;

    protected VirtualFile virtualFile;

    protected String pathToMagento;

    protected UdariSettings settings;


    @Override
    public void actionPerformed(AnActionEvent e) {
        setEvent(e);
        project = getProject();
        settings = UdariSettings.getInstance(project);
        pathToMagento = settings.getPathToMagento();
        if (pathToMagento == null || pathToMagento.isEmpty()) {
            IdeHelper.logError("Cannot find path to Magento 2 installation, make sure you have set the path to Magento 2 in settings");
            return;
        }
        executeAction();
    }

    public void setEvent(AnActionEvent event) {
        reset();
        this.event = event;
    }

    public abstract void executeAction();

    public abstract Boolean isApplicable(AnActionEvent e);

    public AnActionEvent getEvent() {
        return event;
    }

    public DataContext getDataContext() {
        if (dataContext == null) {
            if (getEvent() != null) {
                dataContext = getEvent().getDataContext();
            }
        }
        return dataContext;
    }

    public Project getProject() {
        if (project == null) {
            if (getDataContext() != null) {
                project = PlatformDataKeys.PROJECT.getData(getDataContext());
            }
        }
        return project;
    }


    private void reset() {
        event = null;
        dataContext = null;
        project = null;
    }

    public VirtualFile getVirtualFile() {
        if (virtualFile == null) {
            if (getDataContext() != null) {
                virtualFile = PlatformDataKeys.VIRTUAL_FILE.getData(getDataContext());
            }
        }
        return virtualFile;
    }

    protected void reformatFile(final PsiFile psiFile) {
        if (psiFile != null) {
            final CodeStyleManager codeStyleManager = CodeStyleManager.getInstance(getProject());
            WriteCommandAction.runWriteCommandAction(getProject(), new Runnable() {
                @Override
                public void run() {
                    codeStyleManager.reformat(psiFile);
                }
            });
        }
    }

    public void openFile(VirtualFile file) {
        if (file != null) {
            FileEditorManager.getInstance(getProject()).openFile(file, true);
        }
    }

}
