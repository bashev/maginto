package com.udari.actions;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.*;
import com.jetbrains.php.lang.psi.elements.impl.StringLiteralExpressionImpl;
import com.udari.UdariIcons;
import com.udari.extensions.UdariTemplateFactory;
import com.udari.helpers.Magento;
import com.udari.model.MagentoTheme;
import com.udari.ui.dialog.NewTheme;

import java.io.File;
import java.util.Properties;

/**
 * @author B G Kavinga on 8/6/16.
 */
public class CreateThemeAction extends AbstractAction {

    protected NewTheme dialog;


    public CreateThemeAction() {
        super("Create Theme", "Create new theme", UdariIcons.MAGENTO_ICON_16x16);

    }

    @Override
    protected PsiElement[] invokeDialogImpl(Project project, PsiDirectory directory) {
        dialog = new NewTheme(project);
        dialog.show();
        if (dialog.getExitCode() == DialogWrapper.OK_EXIT_CODE) {
            String companyName = dialog.getCompanyName();
            String themeName = dialog.getThemeName();
            MagentoTheme parentTheme = dialog.getParentTheme();
            createRegisterPhp(themeName, companyName);
            createThemeXml(themeName, companyName, parentTheme);
            createViewXml(themeName, companyName);
        }
        dialog = null;
        return updatedElements;
    }

    protected PsiFile createViewXml(String themeName, String companyName) {
        final Properties properties = new Properties();
        //'frontend/Example/theme',

        final String fileName = "view.xml";

        String directoryPath = pathToMagento + "/app/design/frontend/" + companyName + "/" + themeName + "/etc";
        PsiFile psiFile = UdariTemplateFactory.createFromTemplate(directoryPath,
                properties,
                fileName,
                UdariTemplateFactory.Template.ViewXml,
                project);


        reformatFile(psiFile);

        return psiFile;

    }

    protected PsiFile createThemeXml(String themeName, String companyName, MagentoTheme parentTheme) {

        String parentThemeLocal = null;
        if (parentTheme != null) {
            String themeLocation = parentTheme.getVirtualFile().getParent().getPath();
            VirtualFile registrationPhp = Magento.getFileByPath(project, themeLocation + File.separator + "registration.php");
            PsiFile psiFile = PsiManager.getInstance(project).findFile(registrationPhp);
            ParentFinder pf = new ParentFinder();
            psiFile.accept(pf);
            parentThemeLocal = pf.parenTheme;

        }

        final Properties properties = new Properties();
        properties.setProperty("NAME", companyName + " " + Character.toUpperCase(themeName.charAt(0)) + themeName.substring(1));

        properties.setProperty("PARENT", parentThemeLocal);

        final String fileName = "theme.xml";

        String directoryPath = pathToMagento + "/app/design/frontend/" + companyName + "/" + themeName;
        PsiFile psiFile = UdariTemplateFactory.createFromTemplate(directoryPath,
                properties,
                fileName,
                UdariTemplateFactory.Template.ThemeXml,
                project);


        reformatFile(psiFile);

        return psiFile;

    }

    protected PsiFile createRegisterPhp(String themeName, String companyName) {

        final Properties properties = new Properties();
        //'frontend/Example/theme',
        properties.setProperty("PATH", "frontend/" + companyName + "/" + themeName);
        properties.setProperty("COMPONENTTYPE", "THEME");

        final String fileName = "registration.php";

        String directoryPath = pathToMagento + "/app/design/frontend/" + companyName + "/" + themeName;
        PsiFile psiFile = UdariTemplateFactory.createFromTemplate(directoryPath,
                properties,
                fileName,
                UdariTemplateFactory.Template.RegisterPHP,
                project);


        reformatFile(psiFile);

        return psiFile;

    }

    private class ParentFinder extends PsiRecursiveElementWalkingVisitor {
        public String parenTheme = "";

        @Override
        public void visitElement(PsiElement element) {
            if (element.getClass().getName().equals(StringLiteralExpressionImpl.class.getName())) {
                parenTheme = element.getText();
                if (!parenTheme.equals("")) {
                    String parts[] = parenTheme.split("frontend/");
                    if (parts.length > 0) {
                        parenTheme = parts[1];
                        parenTheme = parenTheme.replaceAll("'", "").replaceAll("\"", "");
                    }
                }
                stopWalking();
            }
            super.visitElement(element);
        }
    }
}
