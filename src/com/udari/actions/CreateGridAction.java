package com.udari.actions;

import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiFile;
import com.intellij.psi.impl.source.xml.XmlFileImpl;
import com.intellij.psi.search.FilenameIndex;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.search.GlobalSearchScopes;
import com.intellij.psi.xml.XmlTag;
import com.udari.UdariSettings;
import com.udari.extensions.UdariTemplateFactory;
import com.udari.ui.dialog.NewGrid;

import java.util.Properties;

/**
 * @author B G Kavinga on 4/13/16.
 */
public class CreateGridAction extends UdariActionAbstract {


    public void executeAction() {

        NewGrid dialog = new NewGrid(project);

        dialog.show();

        if (dialog.isOK()) {
            updateLayoutXml(dialog);
            updateDiXml(dialog);
        }

    }

    private void updateLayoutXml(NewGrid dialog) {
        WriteCommandAction.runWriteCommandAction(getProject(), new XmlUpdater(this, dialog));
    }

    private void updateDiXml(NewGrid dialog) {
        WriteCommandAction.runWriteCommandAction(getProject(), new DiXmlUpdater(this, dialog));
    }

    protected void updateDiXml(String companyName, String moduleName, String layoutXml, String reference) {
        Project project = getProject();
        UdariSettings settings = UdariSettings.getInstance(project);
        // Searching inside app/code directory
        String relativePath = settings.getPathToMagento() + "app/code/" + companyName + "/" + moduleName + "/etc/adminhtml";
        VirtualFile searchScope = project.getBaseDir().findFileByRelativePath(relativePath);
        GlobalSearchScope scope = null;
        PsiFile psiFiles[] = null;
        boolean createNewFile = true;
        if (searchScope != null) {
            scope = GlobalSearchScopes.directoryScope(project, searchScope, true);
            psiFiles = FilenameIndex.getFilesByName(project, "di.xml", scope);
            for (PsiFile psiFile : psiFiles) {
                if (psiFile instanceof XmlFileImpl) {
                    final XmlFileImpl xmlFile = (XmlFileImpl) psiFile;
                    XmlTag rootTag = xmlFile.getRootTag();
                }
            }
        }

        if (createNewFile == true) {
            createDiXml(companyName, moduleName);
        }
    }

    protected PsiFile createDiXml(String companyName, String moduleName) {
        final Properties properties = new Properties();
        properties.setProperty("MODULE", companyName + "_" + moduleName);

        final String fileName = "di.xml";

        String directoryPath = pathToMagento + "/app/code/" + companyName + "/" + moduleName + "/etc/adminhtml";


        PsiFile psiFile = UdariTemplateFactory.createFromTemplate(directoryPath,
                properties,
                fileName,
                UdariTemplateFactory.Template.DiXml,
                getProject());


        reformatFile(psiFile);

        return psiFile;
    }

    protected void updateLayoutXml(String companyName, String moduleName, String layoutXml, String reference) {
        Project project = getProject();
        UdariSettings settings = UdariSettings.getInstance(project);
        // Searching inside app/code directory
        String relativePath = settings.getPathToMagento() + "app/code/" + companyName + "/" + moduleName + "/view/adminhtml/layout/";
        VirtualFile searchScope = project.getBaseDir().findFileByRelativePath(relativePath);
        GlobalSearchScope scope = null;
        PsiFile psiFiles[] = null;
        if (searchScope != null) {
            scope = GlobalSearchScopes.directoryScope(project, searchScope, true);
            psiFiles = FilenameIndex.getFilesByName(project, layoutXml, scope);
            for (PsiFile psiFile : psiFiles) {
                if (psiFile instanceof XmlFileImpl) {
                    final XmlFileImpl xmlFile = (XmlFileImpl) psiFile;
                    XmlTag rootTag = xmlFile.getRootTag();
                    XmlTag containerTag = null;
                    XmlTag uiComponentTag = null;
                    XmlTag bodyTag = null;
                    XmlTag styleTag = null;
                    for (XmlTag tag : rootTag.getSubTags()) {
                        if (tag.getName().equals("update")) {
                            if (tag.getAttribute("handle").getValue().equals("styles")) {
                                styleTag = tag;
                            }
                        }
                        if (tag.getName().equals("body")) {
                            bodyTag = tag;
                            for (XmlTag contentTag : tag.getSubTags()) {
                                if (contentTag.getName().equals("referenceContainer")) {
                                    if (contentTag.getAttribute("name").getValue().equals("content")) {
                                        containerTag = contentTag;
                                        for (XmlTag uiTag : containerTag.getSubTags()) {
                                            if (uiTag.getName().equals("uiComponent")) {
                                                if (uiTag.getAttribute("name").getValue().equals(reference)) {
                                                    uiComponentTag = uiTag;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    // add style tag if not exists
                    if (!(styleTag instanceof XmlTag)) {
                        styleTag = rootTag.createChildTag("update", null, null, false);
                        styleTag.setAttribute("handle", "styles");
                        rootTag.addSubTag(styleTag, false);
                    }

                    // add ui component if not exists
                    if (!(uiComponentTag instanceof XmlTag)) {
                        if (containerTag instanceof XmlTag) {
                            uiComponentTag = containerTag.createChildTag("uiComponent", null, null, false);
                            uiComponentTag.setAttribute("name", reference);
                            containerTag.addSubTag(uiComponentTag, false);
                        } else if (bodyTag instanceof XmlTag) {
                            containerTag = bodyTag.createChildTag("referenceContainer", null, "", false);
                            containerTag.setAttribute("name", "content");
                            uiComponentTag = containerTag.createChildTag("uiComponent", null, null, false);
                            uiComponentTag.setAttribute("name", reference);
                            containerTag.addSubTag(uiComponentTag, false);
                            bodyTag.addSubTag(containerTag, false);
                        } else {
                            bodyTag = rootTag.createChildTag("body", null, "", true);
                            containerTag = bodyTag.createChildTag("referenceContainer", null, "", false);
                            containerTag.setAttribute("name", "content");
                            uiComponentTag = containerTag.createChildTag("uiComponent", null, null, false);
                            uiComponentTag.setAttribute("name", reference);
                            containerTag.addSubTag(uiComponentTag, false);
                            bodyTag.addSubTag(containerTag, false);
                            rootTag.addSubTag(bodyTag, false);

                        }

                    }

                }
            }
        }
    }

    protected class XmlUpdater implements Runnable {

        private CreateGridAction gridAction;
        private NewGrid dialog;

        public XmlUpdater(CreateGridAction gridAction, NewGrid grid) {
            this.gridAction = gridAction;
            this.dialog = grid;
        }

        @Override
        public void run() {
            String moduleName = dialog.getModuleName();
            String companyName = dialog.getCompanyName();
            String layoutXml = dialog.getSelectedHandler();
            String reference = dialog.getReference();
            gridAction.updateLayoutXml(companyName, moduleName, layoutXml, reference);
        }
    }

    protected class DiXmlUpdater implements Runnable {

        private CreateGridAction gridAction;
        private NewGrid dialog;

        public DiXmlUpdater(CreateGridAction gridAction, NewGrid grid) {
            this.gridAction = gridAction;
            this.dialog = grid;
        }

        @Override
        public void run() {
            String moduleName = dialog.getModuleName();
            String companyName = dialog.getCompanyName();
            String layoutXml = dialog.getSelectedHandler();
            String reference = dialog.getReference();
            gridAction.updateDiXml(companyName, moduleName, layoutXml, reference);
        }
    }

    @Override
    public Boolean isApplicable(AnActionEvent e) {
        return true;
    }

}
