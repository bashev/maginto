package com.udari.actions;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.psi.PsiDirectory;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.udari.UdariIcons;
import com.udari.extensions.UdariTemplateFactory;
import com.udari.ui.dialog.NewModuleDialog;

import java.util.Properties;

/**
 * @author B G Kavinga on 2/21/16.
 */
public class CreateModuleAction extends AbstractAction {


    protected NewModuleDialog dialog;


    public CreateModuleAction() {
        super("New Module", "Create New Magento 2 Module", UdariIcons.MAGENTO_ICON_16x16);

    }

    @Override
    protected PsiElement[] invokeDialogImpl(Project project, PsiDirectory directory) {
        dialog = new NewModuleDialog(project);
        dialog.show();
        if (dialog.getExitCode() == DialogWrapper.OK_EXIT_CODE) {
            String companyName = dialog.getCompanyName();
            String moduleName = dialog.getModuleName();
            addUpdatedElement(createModuleXmlFile(moduleName, companyName));
            addUpdatedElement(createRegisterPhp(moduleName, companyName));
            addUpdatedElement(createComposerJSON(moduleName, companyName));
            addUpdatedElement(createDataPhpFile(moduleName, companyName));
            addUpdatedElement(createSchemaPhpFile(moduleName, companyName));
        }
        dialog = null;
        return updatedElements;
    }


    protected PsiFile createModuleXmlFile(String moduleName, String companyName) {

        final Properties properties = new Properties();
        properties.setProperty("MODULENAME", moduleName);
        properties.setProperty("COMPANYNAME", companyName);

        final String fileName = "module.xml";

        String directoryPath = pathToMagento + "/app/code/" + companyName + "/" + moduleName + "/etc";
        PsiFile psiFile = UdariTemplateFactory.createFromTemplate(directoryPath,
                properties,
                fileName,
                UdariTemplateFactory.Template.ModuleXml,
                project);


        reformatFile(psiFile);

        return psiFile;

    }

    protected PsiFile createRegisterPhp(String moduleName, String companyName) {

        final Properties properties = new Properties();
        properties.setProperty("PATH", companyName+"_"+moduleName);
        properties.setProperty("COMPONENTTYPE", "MODULE");

        final String fileName = "registration.php";

        String directoryPath = pathToMagento + "/app/code/" + companyName + "/" + moduleName;
        PsiFile psiFile = UdariTemplateFactory.createFromTemplate(directoryPath,
                properties,
                fileName,
                UdariTemplateFactory.Template.RegisterPHP,
                project);


        reformatFile(psiFile);

        return psiFile;

    }

    protected PsiFile createComposerJSON(String moduleName, String companyName) {

        final Properties properties = new Properties();

        String name = companyName.toLowerCase();
        name += "/module-" + moduleName.toLowerCase();

        String psr4 = companyName + "\\\\" + moduleName + "\\\\";

        properties.setProperty("NAME", name);
        properties.setProperty("PSR4", psr4);

        final String fileName = "composer.json";

        String directoryPath = pathToMagento + "/app/code/" + companyName + "/" + moduleName;
        PsiFile psiFile = UdariTemplateFactory.createFromTemplate(directoryPath,
                properties,
                fileName,
                UdariTemplateFactory.Template.ComposerJSON,
                project);


        return psiFile;

    }

    protected PsiFile createSchemaPhpFile(String moduleName, String companyName) {

        final Properties properties = new Properties();
        properties.setProperty("MODULENAME", moduleName);
        properties.setProperty("COMPANYNAME", companyName);

        final String fileName = "InstallSchema.php";


        properties.setProperty("NAMESPACE", companyName + "\\" + moduleName + "\\" + "Setup");

        String directoryPath = pathToMagento + "/app/code/" + companyName + "/" + moduleName + "/Setup";
        PsiFile psiFile = UdariTemplateFactory.createFromTemplate(directoryPath,
                properties,
                fileName,
                UdariTemplateFactory.Template.InstallSchemaPHP,
                project);


        reformatFile(psiFile);

        return psiFile;

    }

    protected PsiFile createDataPhpFile(String moduleName, String companyName) {

        final Properties properties = new Properties();
        properties.setProperty("MODULENAME", moduleName);
        properties.setProperty("COMPANYNAME", companyName);

        final String fileName = "InstallData.php";


        properties.setProperty("NAMESPACE", companyName + "\\" + moduleName + "\\" + "Setup");

        String directoryPath = pathToMagento + "/app/code/" + companyName + "/" + moduleName + "/Setup";
        PsiFile psiFile = UdariTemplateFactory.createFromTemplate(directoryPath,
                properties,
                fileName,
                UdariTemplateFactory.Template.InstallDataPHP,
                project);


        reformatFile(psiFile);

        return psiFile;

    }


}
