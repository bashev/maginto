package com.udari;

import com.intellij.openapi.actionSystem.*;
import com.intellij.openapi.actionSystem.impl.ActionManagerImpl;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.popup.JBPopupFactory;
import com.intellij.openapi.ui.popup.ListPopup;
import com.intellij.psi.PsiFile;
import com.intellij.psi.util.PsiUtilBase;
import com.udari.actions.UdariActionAbstract;

import java.util.ArrayList;
import java.util.List;

/**
 * @author B G Kavinga on 2/21/16.
 */
public class UdariAction extends UdariActionAbstract{

    public void executeAction() {
        DefaultActionGroup actionGroup = new DefaultActionGroup();
        List<AnAction> actions = _getMagentoContextActions();
        if (actions.size() > 0) {
            for (AnAction action : actions) {
                actionGroup.add(action);
            }
            final ListPopup popup =
                    JBPopupFactory.getInstance().createActionGroupPopup(
                            "Magento 2 Actions",
                            actionGroup,
                            getDataContext(),
                            JBPopupFactory.ActionSelectionAid.SPEEDSEARCH,
                            false);

            popup.showInBestPositionFor(getDataContext());
        }
    }

    /**
     * Define Magento 2 action to be listed when using Alt+B (Option+B)
     *
     * @return
     */
    protected List<AnAction> _getMagentoContextActions() {
        List<AnAction> actions = new ArrayList<AnAction>();
        String[] actionIds = {
                "UdariCreateModule",
                "UdariCreateModel",
                "UdariCreateController",
                "UdariCreateGrid",
                "UdariCopyTemplate",
                //"UdariTest"

        };
        ActionManager actionManager = ActionManagerImpl.getInstance();
        for (String actionId : actionIds) {
            AnAction action = actionManager.getAction(actionId);
            if (((UdariActionAbstract) action).isApplicable(getEvent())) {
                actions.add(action);
            }
        }
        return actions;
    }

    @Override
    public void update(AnActionEvent e) {

        Presentation presentation = e.getPresentation();
        DataContext dataContext = e.getDataContext();

        Project project = PlatformDataKeys.PROJECT.getData(dataContext);
        if (project == null) {
            presentation.setEnabled(false);
            return;
        }

        Editor editor = PlatformDataKeys.EDITOR.getData(dataContext);
        if (editor == null) {
            presentation.setEnabled(false);
            return;
        }

        final PsiFile file = PsiUtilBase.getPsiFileInEditor(editor, project);
        presentation.setEnabled(file != null);
    }

    public Boolean isApplicable(AnActionEvent e) {
        return true;
    }
}
